# python-ipssi

<h1>Découverte des outils - Rédigez un court texte répondant aux questions suivantes</h1>
<h2>Que propose le site PythonAnywhere.com ?</h2>
<br><br>
<p>
    Le site propose gratuitement d'herberger un site python avec l'allocution d'une petite machine. 
    Des comptes payants permettent d'aller plus loin.
</p>
<br><br>
<h2>Qu'est ce que FLASK ? Quels sites connus utilisent Flask ?</h2>
<p>
    FLASK est un petit framework python permettant de mettre en place rapidement un serveur web et très flexible. Beaucoup de 
    sites l'utilisent tels que Pinterest,Lyft,Twilio par exemples
</p>
<br><br>
<h2>Description des actions réalisées</h2>
<h3>Quelles étapes avez-vous suivi ?</h3>
<ol>
    <li>Initialistation du projet git grâce à la commande git init</li>
    <li>Link entre le git local et le server gitlab via SSH grâce à la commande git remote add origin git@gitlab.com:clmb93/python-ipssi.git</li>
    <li>Ajout du code du projet dans le git grâce à la commande git add</li>
    <li>Création du premier commit grâce à la commande git commit -m "Initial commit"</li>
    <li>Installer python</li>
    <li>Avec pip dans cmder faire pip install flask</li>
    <li>Se rend dans le répertoire du projet et utilser cette commande set FLASK_APP=app.py</li>
    <li>Lancer ensuite la commande flask run</li>
    <li>Se rend à l'adresse 127.0.0.1:5000</li>
</ol>
<h2>Quelles difficultés avez-vous rencontrées ?</h2>
<br><br>
<p>
    Lors de l'installation de python 3.8 sur windows pip ne marchait pas. J'ai du tout désinstaller puis réinstaller la version 3.9 pour avoir un pip qui marche.
</p>
<h1>Réflexions sur le projet</h1>
<br><br>
<br><br>
<h2>Quels sont, selon vous, les aspects techniques limitants du projet FLGAZ dans l'état initial ?</h2>
<p>
    Toutes les routes sont dans le même fichier. Il faudrait peut être prévoir de modifier les routes pour les mettres dans un fichier séparé.
</p>
<br><br>
<h2>Quelles sont, selon vous, les menaces auxquelles un tel projet peut être soumis ?</h2>
<p>
    Aucune vérification n'est faîtes lors de l'ajout d'un tweet.
</p>
<br><br><br><br><br>
<h1>Cahier des charges :</h1>
<br><br>
<h2>User : </h2>
<ul>
    <li>inscription</li>
    <li>connexion</li>
    <li>déconnexion</li>
    <li>créér tweet / supprimer tweet</li>
    <li>tweet temporaire - facultatif lors de création</li>
</ul>
<br><br>
<h2>Admin : </h2>
<ul>
    <li>liste user CRUD</li>
    <li>tweet CRUD</li>
</ul>
<br><br>
<h2>Partie Technique : </h2>
<ul>
    <li>protection injection SQL formulaires</li>
    <li>jwt authentification</li>
    <li>hachage mdp</li>
</ul>
