'''
DOCSTRING
'''

import csv
import os
from core.database import mysql
from core.moderation import moderation

from flask import Flask, request, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy

MY_APP = Flask(__name__)
THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
MY_FILE = os.path.join(THIS_FOLDER, "ghost.csv")

MY_APP = Flask(__name__)

#ROUTES

#Home redirect
@MY_APP.route('/')
def acceuil():
    '''
    DOCSTRING
    '''
    return redirect('/timeline')

#Accueil
@MY_APP.route('/timeline', methods=['GET'])
def home():
    '''
    DOCSTRING
    '''
    gaz = parse_from_csv()
    return render_template("home.html", gaz=gaz, username="NULL")

def parse_from_csv():
    '''
    DOCSTRING
    '''
    gaz = []
    with open(MY_FILE, 'r') as my_file:
        reader = csv.reader(my_file)
        for row in reader:
            gaz.append({"user":row[0], "text":row[1]})
    return gaz

def dump_to_csv(form_data):
    '''
    DOCSTRING
    '''
    donnees = [form_data["user-name"], form_data["user-text"]]
    with open(MY_FILE, 'a', newline='', encoding='utf-8') as my_file_csv:
        writer = csv.writer(my_file_csv)
        writer.writerow(donnees)

# Home/user
@MY_APP.route('/timeline/<username>/')
def tweet_form_username(username):
    '''
    DOCSTRING
    '''
    gaz = parse_from_csv()
    return render_template("home.html", gaz=gaz, username=username)


# Login
@MY_APP.route('/login', methods=['POST'])
def login_user():
    '''
    DOCSTRING
    '''
    # passwrd = request.form['user-password']
    # username = request.form['user-name']
    return "A venir"

#Inscription
@MY_APP.route('/inscription', methods=['GET', 'POST'])
def inscription_user():
    '''
    DOCSTRING
    '''
    if request.method == 'POST':
        print(request.form)
        return "ok"
    if request.method == 'GET':
        return render_template('inscription.html')

# Add fantome

@MY_APP.route('/timeline/gaz', methods=['POST'])
def save_gazouille():
    '''
    DOCSTRING
    '''
    gaz = parse_from_csv()
    if len(request.form['user-text']) > 280:
        return render_template("home.html", gaz=gaz, errorMsg='tooLong', username="NULL")
    moderationliste = moderation.LISTE_WORD
    if any(word in request.form['user-text'] for word in moderationliste):
        return render_template("home.html", gaz=gaz, errorMsg='moderationIssue', username="NULL")
    print(request.form)
    dump_to_csv(request.form)
    return redirect(url_for('timeline'))

#USER LIST
@MY_APP.route('/user', methods=['GET'])
def liste_user():
    '''
    DOCSTRING
    '''
    data_base = mysql.connection
    user = data_base.selectTableAll("user")
    return render_template("user.html", liste_user=user)
