from sqlalchemy import create_engine, select, MetaData, Table

class MySQL:
    def __init__(self):
        self.engine = create_engine("mysql+mysqlconnector://root:@localhost/fantome")
        self.metadata = MetaData(bind=None)
        self.connection = self.engine.connect()

    def selectTableAll(self,table):
        table = Table(table, self.metadata, autoload = True, autoload_with = self.engine)
        stmt = select([table])
        return self.connection.execute(stmt).fetchall()
        
    
    